# DVEE - DIVULGAÇÃO DE VAGAS DE EMPREGO PARA EGRESSOS


![](https://gitlab.com/pbaesse/dvee/-/raw/main/app/static/img/logo.png)
![](https://portal.ifrn.edu.br/campus/paudosferros/noticias/proposta-da-lei-orcamentaria-2020-preocupa-ifrn/image_preview)

## Descrição

Esse projeto tem como objetivo desenvolver um sistema que auxilie e facilite o acesso dos egressos do IFRN a oportunidades de emprego através da divulgação de vagas por uma plataforma web. Ele utiliza o framework flask, junto ao javascipt como backend e HTML, CSS e bootstrap para a frontend. 

## Como Instalar e executar
## Começando

### Pré-requisitos

É necessário ter a versão 3.9 do [Python](https://www.python.org/downloads/) instalada em sua máquina.

### Instalação e Execução

É necessário ter a versão mais recente do Python instalada em sua máquina. Para baixar o DVEE, pode ser em duas formas: o arquivo Zip ou o seguindo o comando abaixo em um terminal GIT
Para baixar o DVEE, pode ser em duas formas: o arquivo Zip ou o seguindo o comando abaixo em um terminal GIT

```
git clone https://gitlab.com/pbaesse/estagiar.git
flask run

```
## Desenvolvimento

### Ferramentas utilizadas

* [Python](https://www.python.org/) - Principal linguagem backend utilizada
* [Flask](https://flask.palletsprojects.com/en/2.0.x/) - Framework utilizado
* [jQuery](https://jquery.com/) - Biblioteca javascript


## Problemas e melhorias
### Problemas

Alguns dos problemas que indentificamos ao fazer o sistema, que ainda não foram soluionados, como também sugrestões de melhorias: 
Alguns dos problemas que indentificamos ao fazer o sistema, que ainda não foram soluionados:

- As datas de cadastro e encerramento estão em UTC, em vez de estarem padronizadas com o horário da máquina;
- É possivel cadastrar uma vaga com uma data final anteriror à atual;
- Ainda não existe a verificação do email das empresas;
- Anda não existe um local para a mudança de senha, caso haja esquecimento ou por vontade própria do usuário;
- Ao editar as informações, é possivel deixar o campo vazio;
- Ainda não existe a verificação do email das empresas;
- Não é possivel verificar a autenticidade do CNPJ da empresa;

### Sugestões de Melhorias

Sugestões de funções para implementar ao sistema ao longo das atualizações:

- Criar um local para a mudança de senha, caso haja esquecimento ou por vontade própria do usuário;
- Adicionar sistema de filtro de emails ao importar planilhas;
- Adicionar o status de acompanhamento às vagas;
- Adicionar função de aprovar/recusar candidato;
- Adicionar um sistema de notificações;
- Status de acompanhamento às vagas;
- Função de aprovar/recusar candidato;
- Sistema de notificações;
- Melhorias para a segurança de dados de possiveis invasores.

## Autores e Contato


* **Pedro Baesse Alves Pereira** - *Orientador* - [pbaesse](https://pbaesse.net)
* **Andreza dos Santos Sousa** - *Co-Orientadora*

* **Amanda Gabriela Viana Fabricio** - *Designer* - [AGVF-22](https://github.com/AGVF-22/)
* **Jonathas Gabriel Martins Costa** - *Desenvolvedor* - [Jonathasgmc32](https://gitlab.com/jonathasgmc32) - (jonathasgmc32 at gmail.com)
* **Thalyta Vitoria Freire Oliveira dos Santos** - *Pesquisadora* - [Lyta203](https://github.com/Lyta203/)
