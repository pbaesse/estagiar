from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, DateField, TextAreaField
from flask_wtf.file import FileField, FileAllowed
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length
from app.models import User, Vagas
from flask_login import current_user

class LoginForm(FlaskForm):
    username = StringField('Usuário', validators=[DataRequired()])
    password = PasswordField('Senha', validators=[DataRequired()])
    remember_me = BooleanField('Lembre-me')
    submit = SubmitField('Entrar')

class RegistrationForm(FlaskForm):
    username = StringField('Nome da Empresa:', validators=[DataRequired()])
    email = StringField('Email:', validators=[DataRequired(), Email()])
    matricula_cnpj = StringField('CNPJ:', validators=[DataRequired()])
    password = PasswordField('Senha:', validators=[DataRequired(), Length(min=8, message='A senha deve conter, no mínimo, 8 caracteres')])
    password2 = PasswordField(
        'Confirmar Senha:', validators=[DataRequired(), EqualTo('password', message='As senhas não correspondem')])
    submit = SubmitField('Cadastrar')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Esse usuário já existe')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Esse email já está cadastrado')

    def validate_matricula_cnpj(self, matricula_cnpj):
        user = User.query.filter_by(matricula_cnpj=matricula_cnpj.data).first()
        print(user)
        if user is not None:
            raise ValidationError('CNPJ já está cadastrado')

class VagaForm(FlaskForm):
    titulo = StringField('Título:', validators=[DataRequired()])
    curso = SelectField(u'Selecione o curso-alvo:', choices=[('',''),('Todos', 'Todos'), ('Comércio', 'Comércio'), ('Eletrônica', 'Eletrônica'), ('Informática para Internet', 'Informática para Internet'), ('Marketing', 'Marketing'), ('Manutenção e Suporte', 'Manutenção e Suporte')], validators=[DataRequired()])
    descricao = TextAreaField('Escreva uma descrição:', validators=[DataRequired()])
    prazoFinal = DateField('Data final para as incrições:', validators=[DataRequired()])
    picture = FileField("Deseja adicionar imagem?", validators=[FileAllowed(['jpeg', 'png', 'jpg'], 'Apenas imagens em jpeg e png são aceitas')])
    submit = SubmitField('Cadastrar Vaga')

class PerfilForm(FlaskForm):
    username = StringField('Usuário', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    telefone = StringField('Telefone')
    info = TextAreaField('Descrição')
    picture = FileField('Atualizar Foto', validators=[FileAllowed(['jpeg', 'png', 'jpg'], 'Apenas imagens em jpeg e png são aceitas')])
    curriculo = FileField('Atualizar Currículo', validators=[FileAllowed(['pdf', 'docx', 'txt', 'odt'], 'Apenas os formatos "pdf", "docx", "txt" e "odt" são aceitos')])
    submit = SubmitField('Salvar')

    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(usename=username.data).first()
            if user is not None:
                raise ValidationError('Esse usuário já existe')

    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()
            if user is not None:
                raise ValidationError('Esse email já está cadastrado')

class ImportarForm(FlaskForm):
    arquivo = FileField('Importar Egressos', validators=[FileAllowed(['xls', 'xlsx', 'ods'], 'Apenas os formatos "xls", "xlsx" e "ods" são aceitos'), DataRequired()])
    submit = SubmitField('Enviar')