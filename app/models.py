from datetime import datetime
from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


class User(UserMixin, db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    type_user = db.Column(db.String(128))
    matricula_cnpj = db.Column(db.Integer, unique=True)
    foto_perfil = db.Column(db.String, default='perfil.png')
    info = db.Column(db.String, default='')
    telefone = db.Column(db.String, default='')
    curriculo = db.Column(db.String, default='')
    vagas = db.relationship('Vagas', backref='autor', lazy=True)
    favoritos = db.relationship('Fav_vaga', foreign_keys='Fav_vaga.user_id', backref='user', lazy='dynamic')
    inscricao = db.relationship('Insc_vaga', foreign_keys='Insc_vaga.user_id', backref='user', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def favoritar_vaga(self, vagas):
        if not self.favoritou_vaga(vagas):
            favoritar = Fav_vaga(user_id=self.id, vaga_id=vagas.id)
            db.session.add(favoritar)
    
    def desfavoritar_vaga(self, vagas):
        if self.favoritou_vaga(vagas):
            Fav_vaga.query.filter_by(user_id=self.id, vaga_id=vagas.id).delete()
    
    def favoritou_vaga(self, vagas):
        return Fav_vaga.query.filter(
            Fav_vaga.user_id == self.id,
            Fav_vaga.vaga_id == vagas.id).count() > 0
    
    def candidatar_vaga(self, vagas):
        if not self.candidatou_vaga(vagas):
            candidatar = Insc_vaga(user_id=self.id, vaga_id=vagas.id)
            db.session.add(candidatar)
    
    def descandidatar_vaga(self, vagas):
        if self.candidatou_vaga(vagas):
            Insc_vaga.query.filter_by(user_id=self.id, vaga_id=vagas.id).delete()
    
    def candidatou_vaga(self, vagas):
        return Insc_vaga.query.filter(
            Insc_vaga.user_id == self.id,
            Insc_vaga.vaga_id == vagas.id).count() > 0

    
            
@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class Vagas(db.Model):
    __tablename__ = "vagas"
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String(100))
    curso = db.Column(db.String)
    descricao = db.Column(db.String)
    prazoInicial = db.Column(db.DateTime, default=datetime.now)
    prazoFinal = db.Column(db.DateTime)
    foto_vaga = db.Column(db.String, default='vaga.png')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    favoritos = db.relationship('Fav_vaga', backref='favvaga', lazy='dynamic')
    inscritos = db.relationship('Insc_vaga', backref='inscvaga', lazy='dynamic')

    def __repr__(self):
        return '<Vagas {}>'.format(self.id)

class Fav_vaga(db.Model):
    __tablename__ = "fav_vaga"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    vaga_id = db.Column(db.Integer, db.ForeignKey('vagas.id'))

class Insc_vaga(db.Model):
    __tablename__ = "insc_vaga"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    vaga_id = db.Column(db.Integer, db.ForeignKey('vagas.id'))
